import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
//element-plus按需引入
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  //获取环境变量
  const env = loadEnv(mode, process.cwd())
  return {
    base: './',

    plugins: [
      vue(),
      //element-plus按需引入
      AutoImport({
        resolvers: [ElementPlusResolver()]
      }),
      Components({
        resolvers: [ElementPlusResolver()]
      })
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },

    //跨域；动态环境变量
    server: {
      port: 80,
      open: true,
      // https: false,
      host: '0.0.0.0', // 局域网ip允许外部访问
      proxy: {
        [env.VITE_APP_BASE_API]: {
          target: env.VITE_APP_GAME_SERVER_URL,
          changeOrigin: true,
          rewrite: (path) => path.replace(new RegExp('^' + env.VITE_APP_BASE_API), '')
        }
      }
    }
  }
})
