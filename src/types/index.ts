// 随机推荐：游戏详情接口
export interface IRomInfo {
  id: string
  title: string
  cover: string
  image: string
  url: string
  language: string
  source: string
  comment: string
  size: string
  type: string
  category: string
  publisher: string
  location: string
}

// 轮播图接口
export interface IBanner {
  id: string
  image: string // 图片地址
  title: string
}

//用户信息接口
export interface IUser {
  phone: string
  password: any
  password2?: any
  isLogin?: boolean
  nickname?: string
  favorite?: string[]
  avatar?: string
}
