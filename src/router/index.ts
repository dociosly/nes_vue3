import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/home/HomeView.vue'
import LoginView from '@/views/login/LoginView.vue'
import RegisterView from '@/views/login/RegisterView.vue'
import HomeBody from '@/components/HomeBody.vue'
import GameListBody from '@/components/GameListBody.vue'
import CommentView from '@/views/comment/CommentView.vue'
import PlayBody from '@/components/PlayBody.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      //主页面
      path: '/',
      name: 'home',
      component: HomeView,
      redirect: '/home',
      children: [
        {
          //主页面内容
          path: '/home',
          name: 'homebody',
          component: HomeBody
        },
        {
          //游戏列表页面
          path: '/gamelist',
          name: 'gamelist',
          component: GameListBody
        },
        {
          //游戏页面
          path: '/play',
          name: 'play',
          component: PlayBody
        }
      ]
    },

    {
      //登录页面
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      //注册页面
      path: '/register',
      name: 'register',
      component: RegisterView
    },
    {
      // 评论弹幕页面
      path: '/comment',
      name: 'comment',
      component: CommentView
    }
  ]
})

router.afterEach(() => {
  window.scrollTo(0, 0) //页面滚动到顶部
})

export default router
