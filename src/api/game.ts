import gameAxios from '@/utils/gameAxios'

// 轮播图
export function getBannerList() {
  return gameAxios({
    url: '/banner',
    method: 'GET'
  })
}

//获取游戏分类
export function getCategory() {
  return gameAxios({
    url: '/categorys',
    method: 'GET'
  })
}

/** 获取游戏列表
 * @param cat 分类，比如`ACT`、 `STG`
 * @param keyword 搜索关键字
 * @param page 分页，默认值为1
 * @param limit 每页数量，默认值为20
 */
export function getGameList(page: number, limit: number, cat?: string, keyword?: string) {
  return gameAxios({
    url: '/romlist',
    method: 'GET',
    params: {
      page,
      limit,
      cat,
      keyword
    }
  })
}

/**随机获取游戏列表
 * @param n 游戏数量
 * @param cat 游戏分类，`ACT` 、`STG`
 * @param ignore 不包括某个游戏的id
 * @returns IRomInfo[]
 */
export function getRandom(n?: number, cat?: string, ignore?: string) {
  return gameAxios({
    url: '/random',
    method: 'GET',
    params: {
      n,
      cat,
      ignore
    }
  })
}

//获取单个游戏
export function getID(id: string) {
  return gameAxios({
    url: '/rom',
    method: 'GET',
    params: {
      id
    }
  })
}

//搜索建议
export function getSuggestion(keyword: string) {
  return gameAxios({
    url: '/keyword',
    method: 'GET',
    params: {
      keyword
    }
  })
}

//游戏封面 a ，截图 b
export function getImg(id: number) {
  return gameAxios({
    url: '/roms/img',
    method: 'GET',
    data: {
      id
    }
  })
}

//ROM
export function getRom(title: string) {
  return gameAxios({
    url: '/roms',
    method: 'GET',
    data: {
      title
    }
  })
}
