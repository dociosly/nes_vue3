/**
 * 路由前置守卫
 * 需要在main.ts中注册
 */
import { ElNotification } from 'element-plus'
import router from './router'
import { useUserStore } from './stores/user'

router.beforeEach((to, from, next) => {
  const isLogin = useUserStore().user.isLogin
  if (isLogin) {
    next()
  } else {
    //未登录，拦截游戏页面
    if (to.path === '/gamelist' || to.path === '/play') {
      ElNotification({
        title: '提示',
        message: '请先登录哦😊！',
        type: 'warning'
      })

      next('/login')
    } else {
      next()
    }
  }
})
