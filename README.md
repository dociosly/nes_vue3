## 项目介绍

这是一个基于 Vue.js 的在线红白机游戏项目，使用 TypeScript 进行开发，后端使用作者[taiyuuki](https://github.com/taiyuuki/nes-web/tree/main/server)大大提供的接口，大家多多star他的项目，感谢技术支持。

## 项目展示
![主页](home_page.png)
![游戏列表](game_list.png)
![游戏界面](play.png)

## 项目用到的技术栈
- [Node.js](https://nodejs.org/zh-cn/)
- [typescript](https://www.typescriptlang.org/)
- [Vue.js](https://cn.vuejs.org/)
- [pinia](https://pinia.vuejs.org/zh/)
- [Vue-router](https://router.vuejs.org/zh/)
- [Axios](https://github.com/axios/axios)
- [Element-plus](https://element-plus.org/zh-CN/)
- [Sass](https://sass-lang.com/)
- [NesVue](https://nes-vue-docs.netlify.app/zh/)
- [Danmaku-vue](https://danmaku-vue.dshuais.com/)
- [md5](https://github.com/cotag/ts-md5)

## 项目运行

### 1. 安装依赖包
```sh
npm install
```
### 2. [启动后台服务](https://github.com/taiyuuki/nes-web/tree/main/server)

### 3. 开发环境下编译和热重载

```sh
npm run dev
```

### 生产环境下编译和压缩

```sh
npm run build
```

### 语法检查 [ESLint](https://eslint.org/)

```sh
npm run lint
```
